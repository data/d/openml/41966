# OpenML dataset: isolet

https://www.openml.org/d/41966

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Binarized version of the isolet dataset (see version 1). Only instances with class labels 1 and 2 from the original dataset are considered.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41966) of an [OpenML dataset](https://www.openml.org/d/41966). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41966/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41966/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41966/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

